<!doctype html>
<<<<<<< HEAD
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>@section('title') {{ get_option('site_title') }} @show</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    @section('social-meta')
        <meta property="og:title" content="{{ get_option('site_title') }}">
        <meta property="og:description" content="{{ get_option('meta_description') }}">
        <meta property="og:url" content="{{ route('home') }}">
        <meta name="twitter:card" content="summary_large_image">
        <!--  Non-Essential, But Recommended -->
        <meta name="og:site_name" content="{{ get_option('site_name') }}">
    @show

    <!-- bootstrap css -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-theme.min.css') }}">
    
    <!-- load page specific css -->

    <!-- main select2.css -->
    <link href="{{ asset('assets/select2-3.5.3/select2.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/select2-3.5.3/select2-bootstrap.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('assets/plugins/toastr/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/nprogress/nprogress.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/fonts/stylesheet.css') }}">

    <!-- Conditional page load script -->
    @if(request()->segment(1) === 'dashboard')
        <link rel="stylesheet" href="{{ asset('assets/css/admin.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/plugins/metisMenu/dist/metisMenu.min.css') }}">
        @endif

                <!-- main style.css -->

        <?php $default_style = get_option('default_style'); ?>
        @if($default_style == 'default')
            <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
        @else
            <link rel="stylesheet" href="{{ asset("assets/css/style-{$default_style}.css") }}">
        @endif

        @yield('page-css')

        @if(get_option('additional_css'))
            <style type="text/css">
                {{ get_option('additional_css') }}
            </style>
        @endif

        <script src="{{ asset('assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js') }}"></script><!-- Font awesome 4.4.0 -->
    <link rel="stylesheet" href="{{ asset('assets/font-awesome-4.7.0/css/font-awesome.min.css') }}">
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div class="header-nav-top">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-5 width-100">
                <div class="topContactInfo">
                    <ul class="nav nav-pills">
                            <li>
                                <a href="#">
=======
<!--[if lt IE 7]>      
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="">
   <![endif]-->
   <!--[if IE 7]>         
   <html class="no-js lt-ie9 lt-ie8" lang="">
      <![endif]-->
      <!--[if IE 8]>         
      <html class="no-js lt-ie9" lang="">
         <![endif]-->
         <!--[if gt IE 8]><!--> 
         <html class="no-js" lang="">
            <!--<![endif]-->
            <head>
               <meta charset="utf-8">
               <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
               <title>@section('title') {{ get_option('site_title') }} @show</title>
               <meta name="description" content="">
               <meta name="viewport" content="width=device-width, initial-scale=1">
               @section('social-meta')
               <meta property="og:title" content="{{ get_option('site_title') }}">
               <meta property="og:description" content="{{ get_option('meta_description') }}">
               <meta property="og:url" content="{{ route('home') }}">
               <meta name="twitter:card" content="summary_large_image">
               <!--  Non-Essential, But Recommended -->
               <meta name="og:site_name" content="{{ get_option('site_name') }}">
               @show
               <!-- bootstrap css -->
               <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
               <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-theme.min.css') }}">
               <!-- load page specific css -->
               <!-- main select2.css -->
               <link href="{{ asset('assets/select2-3.5.3/select2.css') }}" rel="stylesheet" />
               <link href="{{ asset('assets/select2-3.5.3/select2-bootstrap.css') }}" rel="stylesheet" />
               <link rel="stylesheet" href="{{ asset('assets/plugins/toastr/toastr.min.css') }}">
               <link rel="stylesheet" href="{{ asset('assets/plugins/nprogress/nprogress.css') }}">
               <link rel="stylesheet" href="{{ asset('assets/fonts/stylesheet.css') }}">
               <!-- Conditional page load script -->
               @if(request()->segment(1) === 'dashboard')
               <link rel="stylesheet" href="{{ asset('assets/css/admin.css') }}">
               <link rel="stylesheet" href="{{ asset('assets/plugins/metisMenu/dist/metisMenu.min.css') }}">
               @endif
               <!-- main style.css -->
               <?php $default_style = get_option('default_style'); ?>
               @if($default_style == 'default')
               <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
               @else
               <link rel="stylesheet" href="{{ asset("assets/css/style-{$default_style}.css") }}">
               @endif
               @yield('page-css')
               @if(get_option('additional_css'))
               <style type="text/css">
                  {{ get_option('additional_css') }}
               </style>
               @endif
               <script src="{{ asset('assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js') }}"></script><!-- Font awesome 4.4.0 -->
               <link rel="stylesheet" href="{{ asset('assets/font-awesome-4.7.0/css/font-awesome.min.css') }}">
            </head>
            <body>
               <!--[if lt IE 8]>
               <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
               <![endif]-->
               <div class="header-nav-top">
                  <div class="container">
                     <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-5 width-100">
                           <div class="topContactInfo">
                              <ul class="nav nav-pills">
                                 <li>
                                    <a href="#">
>>>>>>> a676c9369ad63a676d00a05de1ac8137ef059953
                                    <i class="fa fa-comments" aria-hidden="true"></i>
                                    Live Chat
                                    </a>
                                 </li>
                                 <li>
                                    <a href="#">
                                    <i class="fa fa-bell" aria-hidden="true"></i>
                                    Live Notifications
<<<<<<< HEAD
                                </a>
                            </li>
                    </ul>
                </div>

            </div>
            <div class="col-md-4 col-sm-4 col-xs-7 width-100">
                <div class="topContactInfo">
                    <ul class="nav nav-pills">
                        @if(get_option('site_phone_number'))
                            <li>
                                <a href="callto://+{{get_option('site_phone_number')}}">
=======
                                    </a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-7 width-100">
                           <div class="topContactInfo">
                              <ul class="nav nav-pills">
                                 @if(get_option('site_phone_number'))
                                 <li>
                                    <a href="callto://+{{get_option('site_phone_number')}}">
>>>>>>> a676c9369ad63a676d00a05de1ac8137ef059953
                                    <i class="fa fa-phone"></i>
                                    +{{ get_option('site_phone_number') }}
                                    </a>
                                 </li>
                                 @endif
                                 @if(get_option('site_email_address'))
                                 <li>
                                    <a href="mailto:{{ get_option('site_email_address') }}">
                                    <i class="fa fa-envelope"></i>
                                    {{ get_option('site_email_address') }}
<<<<<<< HEAD
                                </a>
                            </li>
                        @endif
                    </ul>
                </div>

            </div>
            <div class="col-md-4 col-sm-4">
                @if(Auth::check())

                    <div class="topContactInfo">
                        <ul class="nav nav-pills navbar-right">
                            <li>
                                <a href="{{ route('profile') }}">
=======
                                    </a>
                                 </li>
                                 @endif
                              </ul>
                           </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                           @if(Auth::check())
                           <div class="topContactInfo">
                              <ul class="nav nav-pills navbar-right">
                                 <li>
                                    <a href="{{ route('profile') }}">
>>>>>>> a676c9369ad63a676d00a05de1ac8137ef059953
                                    <i class="fa fa-user"></i>
                                    @lang('app.hi'), {{ $logged_user->name }} </a>
                                 </li>
                                 <li>
                                    <a href="{{ route('dashboard') }}">
                                    <i class="fa fa-dashboard"></i>
                                    Dashboard </a>
                                 </li>
                                 <li>
                                    <a href="{{ route('logout') }}">
                                    <i class="fa fa-sign-out"></i>
                                    @lang('app.logout')
                                    </a>
                                 </li>
                              </ul>
                           </div>
                           @else
                           <div class="topContactInfo login_right">
                              <ul class="nav nav-pills navbar-right">
                                 @if($header_menu_pages->count() > 0)
                                 @foreach($header_menu_pages as $page)
                                 <li><a href="{{ route('single_page', $page->slug) }}">{{ $page->title }} </a></li>
                                 @endforeach
                                 @endif
                                 @if( ! Auth::check())
                                 <li><a href="{{ route('user.create') }}"> <i class="fa fa-user-plus" aria-hidden="true"></i>  {{ trans('app.register') }}</a></li>
                                 <li><a href="#">|</a></li>
                                 <li><a href="#" class="nav_text">Are you an Agent?</a></li>
                                 <li><a href="{{ route('login') }}"> <i class="fa fa-lock"></i>  {{ trans('app.login') }}  </a>  </li>
                                 @endif
                              </ul>
                           </div>
                           <!--  {{ Form::open(['route'=>'login','class'=> 'navbar-form navbar-right', 'role'=> 'form']) }}
                              <div class="form-group">
                                 <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="email address">
                              </div>
                              <div class="form-group">
                                 <input  type="password" class="form-control" name="password" placeholder="Password">
                              </div>
                              <button type="submit" class="btn btn-success">@lang('app.sign_in')</button> 
                              {{ Form::close() }} -->
                           @endif
                        </div>
                     </div>
                  </div>
               </div>
<<<<<<< HEAD
                   <!--  {{ Form::open(['route'=>'login','class'=> 'navbar-form navbar-right', 'role'=> 'form']) }}
                    <div class="form-group">
                       <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="email address">
                   </div>
                   <div class="form-group">
                       <input  type="password" class="form-control" name="password" placeholder="Password">
                   </div>
                   <button type="submit" class="btn btn-success">@lang('app.sign_in')</button> 
                   {{ Form::close() }} -->
                @endif

            </div>
        </div>
    </div>

</div>

<nav class="navbar navbar-default" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ route('home') }}">
                @if(get_option('logo_settings') == 'show_site_name')
                    {{ get_option('site_name') }}
                @else
                    @if(logo_url())
=======
               <nav class="navbar navbar-default" role="navigation">
                  <div class="container">
                     <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="{{ route('home') }}">
                        @if(get_option('logo_settings') == 'show_site_name')
                        {{ get_option('site_name') }}
                        @else
                        @if(logo_url())
>>>>>>> a676c9369ad63a676d00a05de1ac8137ef059953
                        <img src="{{ logo_url() }}">
                        @else
                        {{ get_option('site_name') }}
                        @endif
                        @endif
                        </a>
                     </div>
                     <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
                           @if($header_menu_pages->count() > 0)
                           @foreach($header_menu_pages as $page)
                           <li><a href="{{ route('single_page', $page->slug) }}">{{ $page->title }} </a></li>
                           @endforeach
                           @endif
                           <li><a href="../page/maintenance">Maintenance</a></li>
                           <li><a href="../page/properties">Properties</a></li>
                           <li><a href="../page/about-us">About Us</a></li>
                           <li><a href="{{ route('contact_us_page') }}"> @lang('app.contact_us')</a></li>
                           <li class="dropdown hidden-xs">

                              <a href="#" class="dropdown-toggle notify" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="<?php echo asset('/');?>/assets/img/notify.png"" alt="#">Live Notifications</a>
                              <ul class="dropdown-menu notify-drop">
                                <i class="fa fa-sort-asc" aria-hidden="true"></i>
                                 <div class="notify-drop-title">
                                    <div class="row">
                                       <div class="col-md-6 col-sm-6 col-xs-6">Notification</div>
                                       <div class="col-md-6 col-sm-6 col-xs-6 text-right"><a href="" class="rIcon allRead" data-tooltip="tooltip" data-placement="bottom" title="tümü okundu.">Mark All as Read</a></div>
                                    </div>
                                 </div>
                                 <!-- end notify title -->
                                 <!-- notify content -->
                                 <div class="drop-content">
                                    <li>
                                       <i class="fa fa-envelope" aria-hidden="true"></i>
                                       <div class="fleft">
                                          <a href=""><b>Jamie</b></a> has responded to your email 
                                          <p>Good morning Jospeph, Yes, I am inter...</p>
                                          <span>5 Hours Ago | Applicant Tracking</span>
                                       </div>
                                       <a href="" class="rIcon pull-right"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                                    </li>
                                     <hr class="clearfix">
                                     <li>
                                       <i class="fa fa-envelope" aria-hidden="true"></i>
                                       <div class="fleft">
                                          <a href=""><b>Jamie</b></a> has responded to your email 
                                          <p>Good morning Jospeph, Yes, I am inter...</p>
                                          <span>5 Hours Ago | Applicant Tracking</span>
                                       </div>
                                       <a href="" class="rIcon pull-right"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                                    </li>
                                     <hr class="clearfix">
                                     <li>
                                       <i class="fa fa-envelope" aria-hidden="true"></i>
                                       <div class="fleft">
                                          <a href=""><b>Jamie</b></a> has responded to your email 
                                          <p>Good morning Jospeph, Yes, I am inter...</p>
                                          <span>5 Hours Ago | Applicant Tracking</span>
                                       </div>
                                       <a href="" class="rIcon pull-right"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                                    </li>
                                     <hr class="clearfix">
                                 </div>
                                 <div class="notify-drop-footer text-center">
                                    <a href="../page/notification" class="allRead ">See All Notifications</a>
                                 </div>
                                 </li>
                              </ul>
                           <li><a href="#"><i class="fa fa-search" aria-hidden="true"></i></a></li>
                        </ul>
                     </div>
                     <!--/.navbar-collapse -->
                  </div>
               </nav>