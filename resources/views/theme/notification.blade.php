@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<div class="row mlr0">
   <div class="page_wrapper page-{{ $page->id }}">
      <div class="container">
         <div class="col-lg-12">
            <h2 class="single_page_heading">Notification</h2>
            <div class="notification clearfix">
               <div class="notify-n clearfix">
                  <ul class="notify-icons">
                     <li><a href="#"><i class="fa fa-times" aria-hidden="true"></i></a></li>
                     <li><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                  </ul>
                  <div class="notify-des">
                     <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text .</p>
                     <span>5 Hours Ago | Applicant Tracking</span>
                  </div>
                  <div class="pull-right"><em>17/8/18</em></div>
               </div>
               <div class="notify-n clearfix">
                  <ul class="notify-icons">
                     <li><a href="#"><i class="fa fa-times" aria-hidden="true"></i></a></li>
                     <li><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                  </ul>
                  <div class="notify-des">
                     <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text .</p>
                     <span>5 Hours Ago | Applicant Tracking</span>
                  </div>
                  <div class="pull-right"><em>17/8/18</em></div>
               </div>
               <div class="notify-n clearfix">
                  <ul class="notify-icons">
                      <li><a href="#"><i class="fa fa-times" aria-hidden="true"></i></a></li>
                     <li><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                  </ul>
                  <div class="notify-des">
                     <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text .</p>
                     <span>5 Hours Ago | Applicant Tracking</span>
                  </div>
                  <div class="pull-right"><em>17/8/18</em></div>
               </div>
               <div class="notify-n clearfix">
                  <ul class="notify-icons">
                      <li><a href="#"><i class="fa fa-times" aria-hidden="true"></i></a></li>
                     <li><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                  </ul>
                  <div class="notify-des">
                     <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text .</p>
                     <span>5 Hours Ago | Applicant Tracking</span>
                  </div>
                  <div class="pull-right"><em>17/8/18</em></div>
               </div>
               <div class="notify-n clearfix">
                  <ul class="notify-icons">
                      <li><a href="#"><i class="fa fa-times" aria-hidden="true"></i></a></li>
                     <li><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                  </ul>
                  <div class="notify-des">
                     <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text .</p>
                     <span>5 Hours Ago | Applicant Tracking</span>
                  </div>
                  <div class="pull-right"><em>17/8/18</em></div>
               </div>
               <div class="notify-n clearfix">
                  <ul class="notify-icons">
                      <li><a href="#"><i class="fa fa-times" aria-hidden="true"></i></a></li>
                     <li><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                  </ul>
                  <div class="notify-des">
                     <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text .</p>
                     <span>5 Hours Ago | Applicant Tracking</span>
                  </div>
                  <div class="pull-right"><em>17/8/18</em></div>
               </div>
               <div class="learn_more"><a class="btn btn-primary" href="#">Read More</a></div>
            </div>
         </div>
      </div>
      <!-- {!! $page->post_content !!}  -->        
   </div>
</div>
@endsection
@section('page-js')
<script>
   @if(session('success'))
       toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', toastr_options);
   @endif
</script>
@endsection