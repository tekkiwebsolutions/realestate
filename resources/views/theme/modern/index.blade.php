@extends('layout.main')
@section('page-css')
<link rel="stylesheet" href="{{ asset('assets/plugins/owl.carousel/assets/owl.carousel.css') }}">
@endsection
@section('main')
<div class="modern-top-intoduce-section">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <div class="modern-top-hom-cat-section">
               <div class="modern-home-search-bar-wrap">
                  <div class="search-wrapper">
                     <h3> <i class="fa fa-home"></i> @lang('app.find_your_property')</h3>
                     <h6>@lang('app.million_properties')</h6>
                     <form class="form-inline" action="{{ route('listing') }}" method="get">
                        <div class="form-group">
                           <input type="text"  class="form-control" id="searchTerms" name="q" value="{{ request('q') }}" placeholder="@lang('app.search___')" />
                        </div>
                        <div class="form-group">
                           <select class="form-control select2" name="country">
                              <option value="">@lang('app.select_a_country')</option>
                              @foreach($countries as $country)
                              <option value="{{ $country->id }}" {{ request('country') == $country->id ? 'selected' :'' }}>{{ $country->country_name }}</option>
                              @endforeach
                           </select>
                        </div>
                        <div class="form-group">
                           <select class="form-control select2" id="state_select" name="state">
                              <option value=""> @lang('app.select_state') </option>
                           </select>
                        </div>
                        <button type="submit" class="btn theme-btn"> <i class="fa fa-search"></i> @lang('app.search_property')</button>
                     </form>
                     <div class="or-search"> OR </div>
                     <a href="{{ route('listing') }}" class="btn btn-info btn-lg"><i class="fa fa-search-plus"></i> @lang('app.try_advance_search')</a>
                  </div>
               </div>
            </div>
            <div class="clearfix"></div>
         </div>
      </div>
   </div>
</div>
@if($enable_monetize)
<div class="container">
   <div class="row">
      <div class="col-sm-12">
         {!! get_option('monetize_code_below_categories') !!}
      </div>
   </div>
</div>
@endif
@if($urgent_ads->count() > 0)
<div class="container">
   <div class="row">
      <div class="col-sm-12">
         <div class="carousel-header">
            <!-- <h4><a href="{{ route('listing') }}">
               @lang('app.new_urgent_ads') [@lang('app.more') <i class="fa fa-link"></i>]
               </a>
               </h4> -->
            <h3>@lang('app.new_urgent_ads')</h3>
            <h6>@lang('app.listing_properties')</h6>
         </div>
         <div class="themeqx_new_regular_ads_wrap themeqx-carousel-ads">
            @foreach($urgent_ads as $ad)
            <div>
               <div itemscope itemtype="http://schema.org/Product" class="ads-item-thumbnail ad-box-{{$ad->price_plan}}">
                  <div class="ads-thumbnail">
                     <a href="{{ route('single_ad', $ad->slug) }}">
                        <img itemprop="image"  src="{{ media_url($ad->feature_img) }}" class="img-responsive" alt="{{ $ad->title }}">
                        @if($ad->purpose)
                        <span class="modern-sale-rent-indicator">
                        {{ 'For&nbsp;'.ucfirst($ad->purpose) }}
                        </span>
                        @endif
                        <span class="estate_category">
                        Modern Villa
                        </span>
                        <p class="date-posted"> <i class="fa fa-clock-o"></i> {{ $ad->created_at->diffForHumans() }}</p>
                        <p class="price"> <span itemprop="price" content="{{$ad->price}}"> {{ themeqx_price_ng($ad) }} </span></p>
                        <div class="avtar">
                           <img src="assets/img/avtar.jpg" alt="#">
                           <span class="user_name">Grant Barron</span>
                           <span class="modern-img-indicator">
                           @if(! empty($ad->video_url))
                           <span>Video</span>
                           @else
                           <span>{{ $ad->media_img->count() }}&nbsp;Photos</span>
                           @endif
                           </span>
                        </div>
                     </a>
                  </div>
                  <div class="caption">
                     <h4><a href="{{ route('single_ad', $ad->slug) }}" title="{{ $ad->title }}"><span itemprop=name>{{ str_limit($ad->title, 40) }} </span></a></h4>
                     @if($ad->city)
                     <a class="location text-muted" href="{{ route('listing', ['city' => $ad->city->id]) }}"> <i class="fa fa-map-marker"></i> {{ $ad->city->city_name }} </a>
                     @endif
                     <hr/>
                     <table class="table table-responsive property-box-info">
                        <tr>
                           <td> <i class="fa fa-building"></i> {{ ucfirst($ad->type) }} </td>
                           <td><i class="fa fa-arrows-alt "></i>  {{ $ad->square_unit_space.' '.$ad->unit_type }}</td>
                           <td><span class="view"><i class="fa fa-eye" aria-hidden="true"></i>250</span></td>
                        </tr>
                        @if($ad->beds)
                        <tr>
                           <td><i class="fa fa-bed"></i> {{ $ad->beds.' '.trans('app.bedrooms') }}</td>
                           <td><img src="assets/img/floor.png" />{{ $ad->floor.' '.trans('app.floor') }} </td>
                        </tr>
                        @endif
                     </table>
                  </div>
                  <!-- @if($ad->price_plan == 'premium')
                     <div class="ribbon-wrapper-green"><div class="ribbon-green">{{ ucfirst($ad->price_plan) }}</div></div>
                     @endif
                     @if($ad->mark_ad_urgent == '1')
                     <div class="ribbon-wrapper-red"><div class="ribbon-red">@lang('app.urgent')</div></div>
                     @endif -->
               </div>
            </div>
            @endforeach
         </div>
         <!-- themeqx_new_premium_ads_wrap -->
      </div>
   </div>
</div>
@endif
@if($premium_ads->count() > 0)
<div class="premium-bg">
   <div class="container">
      <div class="row">
         <div class="col-sm-12">
            <div class="carousel-header">
               <!-- <h4><a href="{{ route('listing') }}">
                  @lang('app.new_premium_ads') [@lang('app.more') <i class="fa fa-link"></i>]
                  </a>
                  </h4> -->
               <h3>@lang('app.new_premium_ads')</h3>
               <h6>@lang('app.premium_properties')</h6>
            </div>
            <div class="themeqx_new_regular_ads_wrap themeqx-carousel-ads">
               @foreach($premium_ads as $ad)
               <div>
                  <div itemscope itemtype="http://schema.org/Product" class="ads-item-thumbnail ad-box-{{$ad->price_plan}}">
                     <div class="ads-thumbnail">
                        <a href="{{ route('single_ad', $ad->slug) }}">
                           <img itemprop="image"  src="{{ media_url($ad->feature_img) }}" class="img-responsive" alt="{{ $ad->title }}">
                           @if($ad->purpose)
                           <span class="modern-sale-rent-indicator">
                           {{'For&nbsp;'.ucfirst($ad->purpose) }}
                           </span>
                           @endif
                           @if($ad->price_plan == 'premium')
                           <div class="premium">{{ ucfirst($ad->price_plan) }}</div>
                           @endif
                           <p class="date-posted text-muted"> <i class="fa fa-clock-o"></i> {{ $ad->created_at->diffForHumans() }}</p>
                           <p class="price"> <span itemprop="price" content="{{$ad->price}}"> {{ themeqx_price_ng($ad) }} </span></p>
                           <div class="avtar">
                              <img src="assets/img/avtar.jpg" alt="#">
                              <span class="user_name">Grant Barron</span>
                              <span class="modern-img-indicator">
                              @if(! empty($ad->video_url))
                              <span>Video</span>
                              @else
                              <span>{{ $ad->media_img->count() }}&nbsp;Photos</span>
                              @endif
                              </span>
                           </div>
                        </a>
                     </div>
                     <div class="caption">
                        <h4><a href="{{ route('single_ad', $ad->slug) }}" title="{{ $ad->title }}"><span itemprop="name">{{ str_limit($ad->title, 40) }} </span></a></h4>
                        @if($ad->city)
                        <a class="location text-muted" href="{{ route('listing', ['city' => $ad->city->id]) }}"> <i class="fa fa-map-marker"></i> {{ $ad->city->city_name }} </a>
                        @endif
                        <hr/>
                        <table class="table table-responsive property-box-info">
                           <tr>
                              <td><i class="fa fa-building"></i> {{ ucfirst($ad->type) }} </td>
                              <td><i class="fa fa-arrows-alt "></i>  {{ $ad->square_unit_space.' '.$ad->unit_type }}</td>
                              <td><span class="view"><i class="fa fa-eye" aria-hidden="true"></i>450</span></td>
                           </tr>
                           @if($ad->beds)
                           <tr>
                              <td><i class="fa fa-bed"></i> {{ $ad->beds.' '.trans('app.bedrooms') }}</td>
                              <td><img src="assets/img/floor.png" /> {{ $ad->floor.' '.trans('app.floor') }} </td>
                           </tr>
                           @endif
                        </table>
                     </div>
                  </div>
               </div>
               @endforeach
            </div>
            <!-- themeqx_new_premium_ads_wrap -->
         </div>
      </div>
   </div>
</div>
@if($enable_monetize)
<div class="container">
   <div class="row">
      <div class="col-sm-12">
         {!! get_option('monetize_code_below_premium_ads') !!}
      </div>
   </div>
</div>
@endif
@endif
<div class="company_work">
   <div class="container">
      <div class="row">
         <div class="col-md-3 col-xs-6">
            <i class="fa fa-home" aria-hidden="true"></i>
            <div class="col-text">
               <h4>999</h4>
               <h5>Complete Project</h5>
            </div>
<<<<<<< HEAD
        @endif
    @endif


    <div class="company_work">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-xs-6">
                   <i class="fa fa-home" aria-hidden="true"></i>
                   <div class="col-text">
                        <h4>999</h4>
                        <h5>Complete Project</h5>
                   </div>
                </div>
                <div class="col-md-3 col-xs-6">
                   <i class="fa fa-key" aria-hidden="true"></i>
                   <div class="col-text">
                        <h4>720</h4>
                        <h5>Property Sold</h5>
                   </div>
                </div>
                <div class="col-md-3 col-xs-6">
                   <i class="fa fa-smile-o" aria-hidden="true"></i>
                   <div class="col-text">
                        <h4>450</h4>
                        <h5>Happy Clients</h5>
                   </div>
                </div>
                <div class="col-md-3 col-xs-6">
                   <i class="fa fa-trophy" aria-hidden="true"></i>
                   <div class="col-text">
                        <h4>120</h4>
                        <h5>Awards Win</h5>
                   </div>
                </div>
=======
         </div>
         <div class="col-md-3 col-xs-6">
            <i class="fa fa-key" aria-hidden="true"></i>
            <div class="col-text">
               <h4>720</h4>
               <h5>Property Sold</h5>
>>>>>>> a676c9369ad63a676d00a05de1ac8137ef059953
            </div>
         </div>
         <div class="col-md-3 col-xs-6">
            <i class="fa fa-smile-o" aria-hidden="true"></i>
            <div class="col-text">
               <h4>450</h4>
               <h5>Happy Clients</h5>
            </div>
         </div>
         <div class="col-md-3 col-xs-6">
            <i class="fa fa-trophy" aria-hidden="true"></i>
            <div class="col-text">
               <h4>120</h4>
               <h5>Awards Win</h5>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="feature">
   <div class="container">
      <div class="row">
         <div class="col-md-8 col-sm-12">
            <h3>@lang('app.awesome_feature')</h3>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
            <div class="row">
<<<<<<< HEAD
                <div class="col-md-8 col-sm-7">
                    <h3>@lang('app.awesome_feature')</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                    <div class="row">
                        <div class="col-md-4 feature-col">
                            <img src="assets/img/feature.jpg" class="fleft" />
                            <div class="feature-text">
                                <h4>Full Furnished</h4>
                                <p>Lorem Is a dummy text do elud tempor dolor sit</p>
                            </div>
                        </div>
                         <div class="col-md-4 feature-col">
                            <img src="assets/img/feature.jpg" class="fleft" />
                            <div class="feature-text">
                                <h4>Royal Touch Paint</h4>
                                <p>Lorem Is a dummy text do elud tempor dolor sit</p>
                            </div>
                        </div>
                        <div class="col-md-4 feature-col">
                            <img src="assets/img/feature.jpg" class="fleft" />
                            <div class="feature-text">
                                <h4>Latest Interior Design</h4>
                                <p>Lorem Is a dummy text do elud tempor dolor sit</p>
                            </div>
                        </div>
                        <div class="col-md-4 feature-col">
                            <img src="assets/img/feature.jpg" class="fleft" />
                            <div class="feature-text">
                                <h4>Luxurious Fittings</h4>
                                <p>Lorem Is a dummy text do elud tempor dolor sit</p>
                            </div>
                        </div>
                        <div class="col-md-4 feature-col">
                            <img src="assets/img/feature.jpg" class="fleft" />
                            <div class="feature-text">
                                <h4>Living Inside a Nature</h4>
                                <p>Lorem Is a dummy text do elud tempor dolor sit</p>
                            </div>
                        </div>
                        <div class="col-md-4 feature-col">
                            <img src="assets/img/feature.jpg" class="fleft" />
                            <div class="feature-text">
                                <h4>Non Stop Security</h4>
                                <p>Lorem Is a dummy text do elud tempor dolor sit</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-xs-5 feature-img">
                    <img src="assets/img/feature-img.jpg" class="img-responsive" />
                </div>
=======
               <div class="col-sm-4 feature-col">
                  <img src="assets/img/feature.jpg" class="fleft" />
                  <div class="feature-text">
                     <h4>Full Furnished</h4>
                     <p>Lorem Is a dummy text do elud tempor dolor sit</p>
                  </div>
               </div>
               <div class="col-sm-4 feature-col">
                  <img src="assets/img/feature.jpg" class="fleft" />
                  <div class="feature-text">
                     <h4>Royal Touch Paint</h4>
                     <p>Lorem Is a dummy text do elud tempor dolor sit</p>
                  </div>
               </div>
               <div class="col-sm-4 feature-col">
                  <img src="assets/img/feature.jpg" class="fleft" />
                  <div class="feature-text">
                     <h4>Latest Interior Design</h4>
                     <p>Lorem Is a dummy text do elud tempor dolor sit</p>
                  </div>
               </div>
               <div class="col-sm-4 feature-col">
                  <img src="assets/img/feature.jpg" class="fleft" />
                  <div class="feature-text">
                     <h4>Luxurious Fittings</h4>
                     <p>Lorem Is a dummy text do elud tempor dolor sit</p>
                  </div>
               </div>
               <div class="col-sm-4 feature-col">
                  <img src="assets/img/feature.jpg" class="fleft" />
                  <div class="feature-text">
                     <h4>Living Inside a Nature</h4>
                     <p>Lorem Is a dummy text do elud tempor dolor sit</p>
                  </div>
               </div>
               <div class="col-sm-4 feature-col">
                  <img src="assets/img/feature.jpg" class="fleft" />
                  <div class="feature-text">
                     <h4>Non Stop Security</h4>
                     <p>Lorem Is a dummy text do elud tempor dolor sit</p>
                  </div>
               </div>
>>>>>>> a676c9369ad63a676d00a05de1ac8137ef059953
            </div>
         </div>
         <div class="col-md-4 col-xs-5 feature-img">
            <img src="assets/img/feature-img.jpg" class="img-responsive" />
         </div>
      </div>
   </div>
</div>
@endsection
@section('page-js')
<script src="{{ asset('assets/plugins/owl.carousel/owl.carousel.min.js') }}"></script>
<script>
   $(document).ready(function(){
       $(".themeqx_new_premium_ads_wrap").owlCarousel({
           loop: true,
           autoplay:true,
           autoplayTimeout:3000,
           margin:10,
           autoplayHoverPause : true,
           responsiveClass:true,
           responsive:{
               0:{
                   items:1,
                   nav:true,
                   loop:true
               },
               767:{
                   items:2,
                   nav:false,
                   loop:true
               },
               1000:{
                   items:3,
                   nav:true,
                   loop:true
               }
           },
           navText : ['<i class="fa fa-arrow-circle-o-left"></i>','<i class="fa fa-arrow-circle-o-right"></i>']
       });
   });
   
   $(document).ready(function(){
       $(".themeqx_new_regular_ads_wrap").owlCarousel({
           loop: true,
           autoplay : true,
           autoplayTimeout : 2000,
           margin:10,
           autoplayHoverPause : true,
           responsiveClass:true,
           responsive:{
               0:{
                   items:1,
                   nav:true,
                   loop:true
               },
               767:{
                   items:2,
                   nav:false,
                   loop:true
               },
               1000:{
                   items:3,
                   nav:true,
                   loop:true
               }
           },
           navText : ['<i class="fa fa-arrow-circle-o-left"></i>','<i class="fa fa-arrow-circle-o-right"></i>']
       });
   });
   $(document).ready(function(){
       $(".home-latest-blog").owlCarousel({
           loop: true,
           autoplay : true,
           autoplayTimeout : 3000,
           margin:10,
           autoplayHoverPause : true,
           responsiveClass:true,
           responsive:{
               0:{
                   items:1,
                   nav:true,
                   loop:true
               },
               767:{
                   items:2,
                   nav:false,
                   loop:true
               },
               1000:{
                   items:3,
                   nav:true,
                   loop:true
               }
           },
           navText : ['<i class="fa fa-arrow-circle-o-left"></i>','<i class="fa fa-arrow-circle-o-right"></i>']
       });
   });
   
</script>
<script>
   function generate_option_from_json(jsonData, fromLoad){
       //Load Category Json Data To Brand Select
       if(fromLoad === 'country_to_state'){
           var option = '';
           if (jsonData.length > 0) {
               option += '<option value="" selected> @lang('app.select_state') </option>';
               for ( i in jsonData){
                   option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].state_name +' </option>';
               }
               $('#state_select').html(option);
               $('#state_select').select2();
           }else {
               $('#state_select').html('<option value="" selected> @lang('app.select_state') </option>');
               $('#state_select').select2();
           }
           $('#loaderListingIcon').hide('slow');
       }
   }
   
   $(document).ready(function(){
       $('[name="country"]').change(function(){
           var country_id = $(this).val();
           $('#loaderListingIcon').show();
           $.ajax({
               type : 'POST',
               url : '{{ route('get_state_by_country') }}',
               data : { country_id : country_id,  _token : '{{ csrf_token() }}' },
               success : function (data) {
                   generate_option_from_json(data, 'country_to_state');
               }
           });
       });
   });
</script>
@endsection