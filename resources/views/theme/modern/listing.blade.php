@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<div class="modern-top-intoduce-section">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <div class="modern-top-hom-cat-section">
               <div class="modern-home-search-bar-wrap">
                  <div class="search-wrapper advance_search">
                     <h3> <i class="fa fa-home"></i> @lang('app.find_your_property')</h3>
                     <h6>@lang('app.million_properties')</h6>
                     <form class="form-inline" action="{{ route('listing') }}" method="get">
                        <div class="form-group row-width8">
                           <input type="text" class="form-control" name="q" value="{{ request('q') }}" placeholder="@lang('app.search___')" />
                        </div>
                        <button type="submit" class="btn theme-btn"> <i class="fa fa-search"></i> Search</button>
                        <div class="row row-width">
                            <div class="col-sm-3">
                               <div class="form-group">
                                  <select class="form-control select2" name="country">
                                     <option value="">@lang('app.select_a_country')</option>
                                     @foreach($countries as $country)
                                     <option value="{{ $country->id }}" {{ request('country') == $country->id ? 'selected' :'' }}>{{ $country->country_name }}</option>
                                     @endforeach
                                  </select>
                               </div>                                
                              <div class="form-group">
                                   <select class="form-control select2" id="state_select" name="state">
                                      <option value=""> @lang('app.select_state') </option>
                                   </select>
                                </div>    
                               <div class="form-group">
                                   <select class="form-control select2" id="city" name="city">
                                      <option value="">City</option>
                                   </select>
                               </div>                                                                     
                            </div>
                            <div class="col-sm-3">
                               <div class="form-group">
                                  <select class="form-control select2" name="area">
                                     <option value="">Area</option>
                                  </select>
                               </div>                                
                               <div class="form-group">
                                  <select class="form-control select2" name="price">
                                     <option value="">Price From</option>
                                  </select>
                               </div>      
                               <div class="form-group">
                                  <select class="form-control select2" name="price">
                                     <option value="">Price To</option>
                                  </select>
                               </div>                                                                     
                            </div>
                            <div class="col-sm-3">
                               <div class="form-group w-70">
                                  <pre>Sqft</pre>
                               </div>
                               <div class="form-group w-70">
                                  <select class="form-control select2" id="" name="">
                                     <option value="">Sq.</option>
                                     <option value="">4</option>
                                     <option value="">4</option>
                                     <option value="">4</option>
                                  </select>
                               </div>
                               <div class="form-group w-70">
                                  <select class="form-control select2" id="" name="">
                                     <option value="">Ft.</option>
                                     <option value="">2</option>
                                     <option value="">3</option>
                                  </select>
                               </div> 
                                <div class="form-group w-70">
                                  <pre>Bedroom</pre>
                               </div>
                               <div class="form-group w-70">
                                  <select class="form-control select2" id="" name="">
                                     <option value="">1</option>
                                     <option value="">2</option>
                                     <option value="">3</option>
                                     <option value="">4</option>
                                  </select>
                               </div>
                               <div class="form-group w-70">
                                  <select class="form-control select2" id="" name="">
                                     <option value="">5+</option>
                                     <option value="">7</option>
                                     <option value="">8</option>
                                  </select>
                               </div> 
                                <div class="form-group w-70">
                                  <pre>Bathroom</pre>
                               </div>
                               <div class="form-group w-70">
                                  <select class="form-control select2" id="" name="">
                                     <option value="">1</option>
                                     <option value="">2</option>
                                     <option value="">3</option>
                                     <option value="">4</option>
                                  </select>
                               </div>
                               <div class="form-group w-70">
                                  <select class="form-control select2" id="" name="">
                                     <option value="">5+</option>
                                     <option value="">7</option>
                                     <option value="">8</option>
                                  </select>
                               </div>                                        
                            </div>
                            <div class="col-sm-3">
                               <div class="form-group">
                                  <select class="form-control select2" name="property_type">
                                     <option value="">Property Type</option>
                                      <option value="">Apartments</option>
                                      <option value="">Condominiums</option>
                                      <option value="">Serviced Residence</option>
                                      <option value="">Bungalow</option>
                                      <option value="">Semi-detached</option>
                                      <option value="">Cluster House</option>
                                      <option value="">Link House</option>
                                      <option value="">Town House</option>
                                  </select>
                               </div>                                
                                 
                            </div>
                        </div>

                     </form>
                     <!-- <div class="or-search"> OR </div>
                     <a href="{{ route('listing') }}" class="btn btn-info btn-lg"><i class="fa fa-search-plus"></i> @lang('app.try_advance_search')</a> -->
                  </div>
               </div>
            </div>
            <div class="clearfix"></div>
         </div>
      </div>
   </div>
</div>
<div class="container">
   <div class="row">
      <div class="col-md-12">
         <div class="row">
            <div class="col-sm-12">
               <?php
                  $allAdTab = route('listing').str_replace('/', '', str_replace(route('listing'), '', request()->fullUrlWithQuery(['adType'=>'all'])));
                  $personalAdTab = route('listing').str_replace('/', '', str_replace(route('listing'), '', request()->fullUrlWithQuery(['adType'=>'personal'])));
                  $businessAdTab = route('listing').str_replace('/', '', str_replace(route('listing'), '', request()->fullUrlWithQuery(['adType'=>'business'])));
                  
                  ?>
            </div>
         </div>
         @if($enable_monetize)
         <div class="row">
            <div class="col-sm-12">
               {!! get_option('monetize_code_listing_above_premium_ads') !!}
            </div>
         </div>
         @endif
         <div class="ad-box-wrap">
           
            @if($enable_monetize)
            <div class="row">
               <div class="col-sm-12">
                  {!! get_option('monetize_code_listing_above_regular_ads') !!}
               </div>
            </div>
            @endif
            @if($ads->total() > 0)
            <h3>@lang('app.listing_results')</h3>
            <h6>@lang('app.listing_properties')</h6>
            <div class="ad-box-grid-view" style="display: {{ session('grid_list_view') ? (session('grid_list_view') == 'grid'? 'block':'none') : 'block' }};">
               <div class="row">
                  @foreach($ads as $ad)
                  <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                     <div itemscope itemtype="http://schema.org/Product" class="ads-item-thumbnail ad-box-{{$ad->price_plan}}">
                        <div class="ads-thumbnail">
                           <a href="{{ route('single_ad', $ad->slug) }}">
                              <img itemprop="image"  src="{{ media_url($ad->feature_img) }}" class="img-responsive" alt="{{ $ad->title }}">
                              @if($ad->purpose)
                              <span class="modern-sale-rent-indicator">
                              {{'For&nbsp;'.ucfirst($ad->purpose) }}
                              </span>
                              @endif
                              <p class="date-posted text-muted"> <i class="fa fa-clock-o"></i> {{ $ad->created_at->diffForHumans() }}</p>
                              <p class="price"> <span itemprop="price" content="{{$ad->price}}"> {{ themeqx_price_ng($ad) }} </span></p>
                              <div class="avtar">
                                 <img src="assets/img/avtar.jpg" alt="#">
                                 <span class="user_name">Grant Barron</span>
                                 <span class="modern-img-indicator">
                                 @if(! empty($ad->video_url))
                                 <span>Video</span>
                                 @else
                                 <span>{{ $ad->media_img->count() }}&nbsp;Photos</span>
                                 @endif
                                 </span>
                              </div>
                           </a>
                        </div>
                        <div class="caption">
                           <h4><a href="{{ route('single_ad', $ad->slug) }}" title="{{ $ad->title }}"><span itemprop="name">{{ str_limit($ad->title, 40) }} </span></a></h4>
                           @if($ad->city)
                           <a class="location text-muted" href="{{ route('listing', ['city' => $ad->city->id]) }}"> <i class="fa fa-map-marker"></i> {{ $ad->city->city_name }} </a>
                           @endif        
                           <hr/>
                           <table class="table table-responsive property-box-info">
                              <tr>
                                 <td><i class="fa fa-building"></i> {{ ucfirst($ad->type) }} </td>
                                 <td><i class="fa fa-arrows-alt "></i>  {{ $ad->square_unit_space.' '.$ad->unit_type }}</td>
                                 <td><span class="view"><i class="fa fa-eye" aria-hidden="true"></i>450</span></td>
                              </tr>
                              @if($ad->beds)
                              <tr>
                                 <td><i class="fa fa-bed"></i> {{ $ad->beds.' '.trans('app.bedrooms') }}</td>
                                 <td><img src="assets/img/floor.png" /> {{ $ad->floor.' '.trans('app.floor') }} </td>
                              </tr>
                              @endif
                           </table>
                        </div>
                        @if($ad->price_plan == 'premium')
                        <div class="premium">{{ ucfirst($ad->price_plan) }}</div>
                        @endif
                     </div>
                  </div>
                  @endforeach
               </div>
            </div>
            <div class="ad-box-list-view" style="display: {{ session('grid_list_view') == 'list'? 'block':'none' }};">
               <div class="row">
                  <div class="col-sm-12">
                     <table class="table table-bordered table-responsive">
                        @foreach($ads as $ad)
                        <tr class="ad-{{ $ad->price_plan }}">
                           <td width="100">
                              <img itemprop="image"  src="{{ media_url($ad->feature_img) }}" class="img-responsive" alt="{{ $ad->title }}">
                              <span class="modern-img-indicator">
                              @if(! empty($ad->video_url))
                              <i class="fa fa-file-video-o"></i>
                              @else
                              <i class="fa fa-file-image-o"> {{ $ad->media_img->count() }}</i>
                              @endif
                              </span>
                           </td>
                           <td>
                              <h5><a href="{{ route('single_ad', $ad->slug) }}" >{{ $ad->title }}</a> </h5>
                              <h5>{{ themeqx_price_ng($ad) }} </h5>
                              <p class="text-muted">
                                 @if($ad->city)
                                 <i class="fa fa-map-marker"></i> <a class="location text-muted" href="{{ route('listing', ['city'=>$ad->city->id]) }}"> {{ $ad->city->city_name }} </a>,
                                 @endif
                                 <i class="fa fa-clock-o"></i> {{ $ad->created_at->diffForHumans() }}
                              </p>
                              <p class="listViewItemFooter">
                                 <span> <i class="fa fa-building"></i> {{ ucfirst($ad->type) }} </span>
                                 <span> <i class="fa fa-arrows-alt "></i>  {{ $ad->square_unit_space.' '.$ad->unit_type }} </span>
                                 @if($ad->beds)
                                 <span>
                                 <i class="fa fa-bed"></i> {{ $ad->beds.' '.trans('app.bedrooms') }} </span>
                                 <span>{{ $ad->floor.' '.trans('app.floor') }}</span>
                                 @endif
                              </p>
                              @if($ad->price_plan == 'premium')
                              <div class="ribbon-green-bar">{{ ucfirst($ad->price_plan) }}</div>
                              @endif
                              @if($ad->mark_ad_urgent == '1')
                              <div class="ribbon-red-bar">@lang('app.urgent')</div>
                              @endif
                           </td>
                        </tr>
                        @endforeach
                     </table>
                  </div>
               </div>
            </div>
            @else
            <div class="alert alert-warning">
               <h2><i class="fa fa-info-circle"></i> @lang('app.search_not_found') </h2>
            </div>
            @endif
            @if($enable_monetize)
            <div class="row">
               <div class="col-sm-12">
                  {!! get_option('monetize_code_listing_below_regular_ads') !!}
               </div>
            </div>
            @endif
         </div>
         <div class="row">
            <div class="col-xs-12">
               {{ $ads->appends(request()->input())->links() }}
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('page-js')
<script>
   $(document).ready(function() {
       $('#shortBySelect').change(function () {
           var form_input = $('#listingFilterForm').serialize();
           location.href = '{{ route('listing') }}?' + form_input + '&shortBy=' + $(this).val();
       });
   });
   function generate_option_from_json(jsonData, fromLoad){
       //Load Category Json Data To Brand Select
       if (fromLoad === 'category_to_sub_category'){
           var option = '';
           if (jsonData.length > 0) {
               option += '<option value="" selected> <?php echo trans('app.select_a_sub_category') ?> </option>';
               for ( i in jsonData){
                   option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].category_name +' </option>';
               }
               $('#sub_category_select').html(option);
               $('#sub_category_select').select2();
           }else {
               $('#sub_category_select').html('<option value="">@lang('app.select_a_sub_category')</option>');
               $('#sub_category_select').select2();
           }
           $('#loaderListingIcon').hide('slow');
       }else if (fromLoad === 'category_to_brand'){
           var option = '';
           if (jsonData.length > 0) {
               option += '<option value="" selected> <?php echo trans('app.select_a_brand') ?> </option>';
               for ( i in jsonData){
                   option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].brand_name +' </option>';
               }
               $('#brand_select').html(option);
               $('#brand_select').select2();
           }else {
               $('#brand_select').html('<option value="">@lang('app.select_a_brand')</option>');
               $('#brand_select').select2();
           }
           $('#loaderListingIcon').hide('slow');
       }else if(fromLoad === 'country_to_state'){
           var option = '';
           if (jsonData.length > 0) {
               option += '<option value="" selected> @lang('app.select_state') </option>';
               for ( i in jsonData){
                   option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].state_name +' </option>';
               }
               $('#state_select').html(option);
               $('#state_select').select2();
           }else {
               $('#state_select').html('<option value="" selected> @lang('app.select_state') </option>');
               $('#state_select').select2();
           }
           $('#loaderListingIcon').hide('slow');
   
       }else if(fromLoad === 'state_to_city'){
           var option = '';
           if (jsonData.length > 0) {
               option += '<option value="" selected> @lang('app.select_city') </option>';
               for ( i in jsonData){
                   option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].city_name +' </option>';
               }
               $('#city_select').html(option);
               $('#city_select').select2();
           }else {
               $('#city_select').html('<option value="" selected> @lang('app.select_city') </option>');
               $('#city_select').select2();
           }
           $('#loaderListingIcon').hide('slow');
       }
   }
   
   $(function(){
   
       $('[name="country"]').change(function(){
           var country_id = $(this).val();
           $('#loaderListingIcon').show();
           $.ajax({
               type : 'POST',
               url : '{{ route('get_state_by_country') }}',
               data : { country_id : country_id,  _token : '{{ csrf_token() }}' },
               success : function (data) {
                   generate_option_from_json(data, 'country_to_state');
               }
           });
       });
       $('[name="state"]').change(function(){
           var state_id = $(this).val();
           $('#loaderListingIcon').show();
           $.ajax({
               type : 'POST',
               url : '{{ route('get_city_by_state') }}',
               data : { state_id : state_id,  _token : '{{ csrf_token() }}' },
               success : function (data) {
                   generate_option_from_json(data, 'state_to_city');
               }
           });
       });
   });
   $(function(){
       $('#showGridView').click(function(){
           $('.ad-box-grid-view').show();
           $('.ad-box-list-view').hide();
           $.ajax({
               type : 'POST',
               url : '{{ route('switch_grid_list_view') }}',
               data : { grid_list_view : 'grid',  _token : '{{ csrf_token() }}' },
           });
       });
       $('#showListView').click(function(){
           $('.ad-box-grid-view').hide();
           $('.ad-box-list-view').show();
           $.ajax({
               type : 'POST',
               url : '{{ route('switch_grid_list_view') }}',
               data : { grid_list_view : 'list',  _token : '{{ csrf_token() }}' },
           });
       });
   });
</script>
<script>
   @if(session('success'))
       toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', toastr_options);
   @endif
</script>
@endsection