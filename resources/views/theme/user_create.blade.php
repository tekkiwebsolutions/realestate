@extends('layout.main')
@section('title') Register | @parent @endsection
@section('main')
<<<<<<< HEAD

  
=======
<<<<<<< HEAD
<div class="container">
   <div class="row">
      <div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3 col-xs-12">
         <div class="login">
            <h3 class="authTitle">Sign up or <a href="{{ route('login') }}">@lang('app.login')</a></h3>
            @if(session('error'))
            <div class="alert alert-danger">
               {{ session('error') }}
            </div>
            @endif
            {{ Form::open(['route'=>'user.store', 'role'=> 'form']) }}
            <div class="row">
               <div class="col-xs-12 col-sm-6 col-md-6">
                  <div class="form-group {{ $errors->has('first_name')? 'has-error':'' }} ">
                     <input type="text" name="first_name" id="first_name" class="form-control" value="{{ old('first_name') }}" placeholder="First Name" tabindex="1">
                     {!! $errors->has('first_name')? '
                     <p class="help-block">'.$errors->first('first_name').'</p>
                     ':'' !!}
                  </div>
               </div>
               <div class="col-xs-12 col-sm-6 col-md-6">
                  <div class="form-group {{ $errors->has('last_name')? 'has-error':'' }} ">
                     <input type="text" name="last_name" id="last_name" class="form-control"  value="{{ old('last_name') }}" placeholder="Last Name" tabindex="2">
                     {!! $errors->has('last_name')? '
                     <p class="help-block">'.$errors->first('last_name').'</p>
                     ':'' !!}
                  </div>
               </div>
            </div>
            <div class="form-group {{ $errors->has('email')? 'has-error':'' }} ">
               <input type="email" name="email" id="email" class="form-control" value="{{ old('email') }}" placeholder="Email Address" tabindex="4">
               {!! $errors->has('email')? '
               <p class="help-block">'.$errors->first('email').'</p>
               ':'' !!}
            </div>
            <div class="form-group {{ $errors->has('phone')? 'has-error':'' }}">
               <input type="text" name="phone" id="phone" class="form-control" value="{{ old('phone') }}" placeholder="Phone Number" tabindex="3">
               {!! $errors->has('phone')? '
               <p class="help-block">'.$errors->first('phone').'</p>
               ':'' !!}
            </div>
            <div class="row">
               <div class="col-xs-12">
                  <div class="form-group {{ $errors->has('gender')? 'has-error':'' }}">
                     <select id="gender" name="gender" class="form-control select2">
                        <option value="">Select Gender</option>
                        <option value="male">Male</option>
                        <option value="female">Fe-Male</option>
                        <option value="third_gender">Third Gender</option>
                     </select>
                     {!! $errors->has('gender')? '
                     <p class="help-block">'.$errors->first('gender').'</p>
                     ':'' !!}
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  <div class="form-group {{ $errors->has('country')? 'has-error':'' }}">
                     <select id="country" name="country" class="form-control select2">
                        <option value="">@lang('app.select_a_country')</option>
                        @foreach($countries as $country)
                        <option value="{{ $country->id }}" {{ old('country') == $country->id ? 'selected' :'' }}>{{ $country->country_name }}</option>
                        @endforeach
                     </select>
                     {!! $errors->has('country')? '
                     <p class="help-block">'.$errors->first('country').'</p>
                     ':'' !!}
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12 col-sm-6 col-md-6">
                  <div class="form-group {{ $errors->has('password')? 'has-error':'' }}">
                     <input type="password" name="password" id="password" class="form-control" placeholder="Password" tabindex="5">
                     {!! $errors->has('password')? '
                     <p class="help-block">'.$errors->first('password').'</p>
                     ':'' !!}
                  </div>
               </div>
               <div class="col-xs-12 col-sm-6 col-md-6">
                  <div class="form-group {{ $errors->has('password_confirmation')? 'has-error':'' }}">
                     <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Confirm Password" tabindex="6">
                     {!! $errors->has('password_confirmation')? '
                     <p class="help-block">'.$errors->first('password_confirmation').'</p>
                     ':'' !!}
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12 col-sm-6 col-md-6">
                  <div class="form-group">
                     <input type="text" name="city" id="city" class="form-control" placeholder="City" tabindex="7">
                  </div>
               </div>
               <div class="col-xs-12 col-sm-6 col-md-6">
                  <div class="form-group">
                     <input type="text" name="state" id="state" class="form-control" placeholder="State" tabindex="8">
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12 col-sm-6 col-md-6">
                  <div class="form-group">
                     <input type="text" name="company" id="company" class="form-control" placeholder="Company" tabindex="9">
                  </div>
               </div>
               <div class="col-xs-12 col-sm-6 col-md-6">
                  <div class="form-group">
                     <input type="text" name="postal_code" id="postal_code" class="form-control" placeholder="Postal Code" tabindex="10">
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12 col-sm-6 col-md-6">
                  <div class="form-group">
                     <textarea name="address" id="address" class="form-control" placeholder="Address(Work/Home)"></textarea>
                  </div>
               </div>
               <div class="col-xs-12 col-sm-6 col-md-6">
                  <div class="form-group">
                     <textarea name="description" id="description" class="form-control" placeholder="Description"></textarea>
                  </div>
               </div>
            </div>
            <div class="row  {{ $errors->has('password')? 'has-error':'' }}">
               <div class="col-xs-4 col-sm-3 col-md-3">
                  <span class="button-checkbox">
                  <label><input type="checkbox" name="agree" value="1" /> I Agree </label>
                  </span>
               </div>
               <div class="col-xs-8 col-sm-9 col-md-9">
                  <b>By clicking <strong class="label label-primary">Register</strong>, you agree to the <a href="{{ route('single_page', 'terms-and-condition') }}" target="_blank">Terms and Conditions</a> set out by this site, including our Cookie Use.</b>
               </div>
               <div class="col-sm-12">
                  {!! $errors->has('password')? '
                  <p class="help-block">You must agree with terms and condition</p>
                  ':'' !!}
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12"><input type="submit" value="Register" class="btn btn-primary m-t-20 btn-block btn-lg" tabindex="7"></div>
            </div>
            {{ Form::close() }}
         </div>
      </div>
   </div>
</div>
=======

  <style type="text/css">
      .dropdown {
    margin: 10px 0;
    padding: 10px 0;
    border-bottom: 1px solid #DDD;
}

.dropdown span {
    display:inline-block;
    width: 80px;
}
  </style>
>>>>>>> a676c9369ad63a676d00a05de1ac8137ef059953
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3 col-xs-12">

                <div class="login">
                    <h3 class="authTitle">Sign up or <a href="{{ route('login') }}">@lang('app.login')</a></h3>

                    @if(session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif

                    {{ Form::open(['route'=>'user.store', 'role'=> 'form']) }}
<<<<<<< HEAD
                   
=======
>>>>>>> a676c9369ad63a676d00a05de1ac8137ef059953
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group {{ $errors->has('first_name')? 'has-error':'' }} ">
                                <input type="text" name="first_name" id="first_name" class="form-control" value="{{ old('first_name') }}" placeholder="First Name" tabindex="1">

                                {!! $errors->has('first_name')? '<p class="help-block">'.$errors->first('first_name').'</p>':'' !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group {{ $errors->has('last_name')? 'has-error':'' }} ">
                                <input type="text" name="last_name" id="last_name" class="form-control"  value="{{ old('last_name') }}" placeholder="Last Name" tabindex="2">
                                {!! $errors->has('last_name')? '<p class="help-block">'.$errors->first('last_name').'</p>':'' !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group {{ $errors->has('email')? 'has-error':'' }} ">
                        <input type="email" name="email" id="email" class="form-control" value="{{ old('email') }}" placeholder="Email Address" tabindex="3">
                        {!! $errors->has('email')? '<p class="help-block">'.$errors->first('email').'</p>':'' !!}

                    </div>

                    <div class="form-group {{ $errors->has('phone')? 'has-error':'' }}">
                        <input type="text" name="phone" id="phone" class="form-control" value="{{ old('phone') }}" placeholder="Phone Number" tabindex="4">
                        {!! $errors->has('phone')? '<p class="help-block">'.$errors->first('phone').'</p>':'' !!}
                    </div>


                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group {{ $errors->has('gender')? 'has-error':'' }}">
                                <select id="gender" name="gender" class="form-control select2" tabindex="5">
                                    <option value="">Select Gender</option>
                                    <option value="male">Male</option>
                                    <option value="female">Fe-Male</option>
                                    <option value="third_gender">Third Gender</option>
                                </select>
                                {!! $errors->has('gender')? '<p class="help-block">'.$errors->first('gender').'</p>':'' !!}

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group {{ $errors->has('password')? 'has-error':'' }}">
                                <input type="password" name="password" id="password" class="form-control" placeholder="Password" tabindex="7">
                                {!! $errors->has('password')? '<p class="help-block">'.$errors->first('password').'</p>':'' !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group {{ $errors->has('password_confirmation')? 'has-error':'' }}">
                                <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Confirm Password" tabindex="8">
                                {!! $errors->has('password_confirmation')? '<p class="help-block">'.$errors->first('password_confirmation').'</p>':'' !!}

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group {{ $errors->has('address')? 'has-error':'' }}">
                                <textarea name="address" id="address" class="form-control" placeholder="Address(Work/Home)" tabindex="13"></textarea>
                                {!! $errors->has('address')? '<p class="help-block">'.$errors->first('address').'</p>':'' !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group {{ $errors->has('country')? 'has-error':'' }}">
                                <select id="country" name="country" class="form-control select2" tabindex="6">
                                    <option value="">@lang('app.select_a_country')</option>
                                    @foreach($countries as $country)
                                        <option value="{{ $country->id }}" {{ old('country') == $country->id ? 'selected' :'' }}>{{ $country->country_name }}</option>
                                    @endforeach
                                </select>
                                {!! $errors->has('country')? '<p class="help-block">'.$errors->first('country').'</p>':'' !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
<<<<<<< HEAD
                            <div class="form-group {{ $errors->has('password')? 'has-error':'' }}">
                                <input type="password" name="password" id="password" class="form-control" placeholder="Password" tabindex="5">
                                {!! $errors->has('password')? '<p class="help-block">'.$errors->first('password').'</p>':'' !!}
=======
                            <div class="form-group  {{ $errors->has('state')? 'has-error':'' }}">
                                
                                    <select class="form-control select2" id="state_select" name="state">
                                        @if($previous_states->count() > 0)
                                            @foreach($previous_states as $state)
                                                <option value="{{ $state->id }}" {{ old('state') == $state->id ? 'selected' :'' }}>{{ $state->state_name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    <p class="text-info">
                                        <span id="state_loader" style="display: none;"><i class="fa fa-spin fa-spinner"></i> </span>
                                    </p>
                                
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            
                            <div class="form-group  {{ $errors->has('city')? 'has-error':'' }}">
                                
                                    <select class="form-control select2" id="city_select" name="city">
                                        @if($previous_cities->count() > 0)
                                            @foreach($previous_cities as $city)
                                                <option value="{{ $city->id }}" {{ old('city') == $city->id ? 'selected':'' }}>{{ $city->city_name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    <p class="text-info">
                                        <span id="city_loader" style="display: none;"><i class="fa fa-spin fa-spinner"></i> </span>
                                    </p>
                                
                            </div>
                        </div>
                    </div>
                    
                     
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group {{ $errors->has('postal_code')? 'has-error':'' }}">
                                <input type="text" name="postal_code" id="postal_code" class="form-control" placeholder="Enter Postal code" tabindex="12">
                                {!! $errors->has('postal_code')? '<p class="help-block">'.$errors->first('postal_code').'</p>':'' !!}

>>>>>>> a676c9369ad63a676d00a05de1ac8137ef059953
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group {{ $errors->has('company')? 'has-error':'' }}">
                                <input type="text" name="company" id="company" class="form-control" placeholder="Enter company name" tabindex="11">
                                {!! $errors->has('company')? '<p class="help-block">'.$errors->first('company').'</p>':'' !!}

                            </div>
                        </div>
                    </div>
<<<<<<< HEAD
                     <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <input type="text" name="city" id="city" class="form-control" placeholder="City" tabindex="7">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <input type="text" name="state" id="state" class="form-control" placeholder="State" tabindex="8">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <input type="text" name="company" id="company" class="form-control" placeholder="Company" tabindex="9">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <input type="text" name="postal_code" id="postal_code" class="form-control" placeholder="Postal Code" tabindex="10">
                            </div>
                        </div>
                    </div>
                     <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <textarea name="address" id="address" class="form-control" placeholder="Address(Work/Home)"></textarea>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <textarea name="description" id="description" class="form-control" placeholder="Description"></textarea>
=======

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group {{ $errors->has('description')? 'has-error':'' }}">
                                <textarea name="description" id="description" class="form-control" placeholder="Enter description" tabindex="14"></textarea>
                                {!! $errors->has('description')? '<p class="help-block">'.$errors->first('description').'</p>':'' !!}
>>>>>>> a676c9369ad63a676d00a05de1ac8137ef059953
                            </div>
                        </div>
                    </div>
                    <div class="row  {{ $errors->has('password')? 'has-error':'' }}">
                        <div class="col-xs-4 col-sm-3 col-md-3">
                    <span class="button-checkbox">
                        <label><input type="checkbox" name="agree" value="1" tabindex="15" /> I Agree </label>
                    </span>
                        </div>
                        <div class="col-xs-8 col-sm-9 col-md-9">
                            <b>By clicking <strong class="label label-primary">Register</strong>, you agree to the <a href="{{ route('single_page', 'terms-and-condition') }}" target="_blank">Terms and Conditions</a> set out by this site, including our Cookie Use.</b>
                        </div>

                        <div class="col-sm-12">
                            {!! $errors->has('password')? '<p class="help-block">You must agree with terms and condition</p>':'' !!}
                        </div>
                    </div>
<<<<<<< HEAD

                    <div class="row">
                        <div class="col-xs-12"><input type="submit" value="Register" class="btn btn-primary m-t-20 btn-block btn-lg" tabindex="7"></div>
=======
                    <div class="row">
                        <div class="col-xs-12"><input type="submit" value="Register" class="btn btn-primary m-t-20 btn-block btn-lg" tabindex="16"></div>
>>>>>>> a676c9369ad63a676d00a05de1ac8137ef059953
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
>>>>>>> ec18a954915501b40ae2fc34ad35a5ef045f00ae
@endsection
@section('page-js')
<<<<<<< HEAD
<script>
   $(document).ready(function() {
       $('#phone').keyup(function(){
           $(this).val($(this).val().replace(/[^0-9]/g,""));
       });
   
       $(".notify img").attr("src", "<?php echo asset('/');?>/assets/img/notify.png");
       $(".footer-widget img").attr("src", "<?php echo asset('/');?>/assets/img/footer-logo.png");
       
   });
   
</script>
@endsection
=======
    <script>
        function generate_option_from_json(jsonData, fromLoad){
            //Load Category Json Data To Brand Select
            if (fromLoad === 'category_to_brand'){
                var option = '';
                if (jsonData.length > 0) {
                    option += '<option value="0" selected> <?php echo trans('app.select_a_brand') ?> </option>';
                    for ( i in jsonData){
                        option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].brand_name +' </option>';
                    }
                    $('#brand_select').html(option);
                    $('#brand_select').select2();
                }else {
                    $('#brand_select').html('');
                    $('#brand_select').select2();
                }
                $('#brand_loader').hide('slow');
            }else if(fromLoad === 'country_to_state'){
                var option = '';
                if (jsonData.length > 0) {
                    option += '<option value="0" selected> @lang('app.select_state') </option>';
                    for ( i in jsonData){
                        option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].state_name +' </option>';
                    }
                    $('#state_select').html(option);
                    $('#state_select').select2();
                }else {
                    $('#state_select').html('');
                    $('#state_select').select2();
                }
                $('#state_loader').hide('slow');

            }else if(fromLoad === 'state_to_city'){
                var option = '';
                if (jsonData.length > 0) {
                    option += '<option value="0" selected> @lang('app.select_city') </option>';
                    for ( i in jsonData){
                        option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].city_name +' </option>';
                    }
                    $('#city_select').html(option);
                    $('#city_select').select2();
                }else {
                    $('#city_select').html('');
                    $('#city_select').select2();
                }
                $('#city_loader').hide('slow');
            }
        }
        $(document).ready(function() {
            $('#phone').keyup(function(){
                $(this).val($(this).val().replace(/[^0-9]/g,""));
            });

            $(".notify img").attr("src", "<?php echo asset('/');?>/assets/img/notify.png");
            $(".footer-widget img").attr("src", "<?php echo asset('/');?>/assets/img/footer-logo.png");
            
<<<<<<< HEAD
=======
        });
        $('[name="country"]').change(function(){
            var country_id = $(this).val();
            $('#state_loader').show();
            $.ajax({
                type : 'POST',
                url : '{{ route('get_state_by_country') }}',
                data : { country_id : country_id,  _token : '{{ csrf_token() }}' },
                success : function (data) {
                    generate_option_from_json(data, 'country_to_state');
                }
            });
>>>>>>> a676c9369ad63a676d00a05de1ac8137ef059953
        });

        $('[name="state"]').change(function(){
            var state_id = $(this).val();
            $('#city_loader').show();
            $.ajax({
                type : 'POST',
                url : '{{ route('get_city_by_state') }}',
                data : { state_id : state_id,  _token : '{{ csrf_token() }}' },
                success : function (data) {
                    generate_option_from_json(data, 'state_to_city');
                }
            });
        });
    </script>
@endsection

>>>>>>> ec18a954915501b40ae2fc34ad35a5ef045f00ae
