@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<div class="row mlr0">
   <div class="page_wrapper page-{{ $page->id }}">
      <div class="modern-top-intoduce-section banner">
         <div class="container">
            <div class="row">
               <div class="col-sm-12 col-lg-12">
                  <h2>Agent Detail</h2>
               </div>
            </div>
         </div>
      </div>
      <div class="about_us single-agent_detail">
         <div class="container">
            <div class="row clearfix">
               <div class="col-sm-3 agent-avtar">
                  <div class="avtar"><img src="http://localhost/realestate/assets/img/marcus.png" /></div>
                  <ul class="agent_profiles">
                     <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                     <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                     <li><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                  </ul>
                  <div><a class="btn btn-primary contact-btn" href="#">Contact</a></div>
               </div>
               <div class="col-sm-5 agent-detail-manual">
                  <h2>Marcus Marrio</h2>
                  <span>Sale Assocaite - (Property Lookout)</span>
                  <ul>
                     <li><a href="callto:+60194526523">+60194526523</a></li>
                     <li><a href="mailto:marrio.sa@gmail.com">marrio.sa@gmail.com</a></li>
                     <li>Lot 4452, Taman Damansara, Kuching, Sarawak</li>
                     <li><a href="#">www.marcusmarrio.com.my</a></li>
                  </ul>
                  <table>
                     <tbody>
                        <tr>
                           <td>Gender:</td>
                           <td>Male</td>
                        </tr>
                        <tr>
                           <td>Country:</td>
                           <td>Malaysia</td>
                        </tr>
                        <tr>
                           <td>Company:</td>
                           <td>ABC Estate</td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </div>
            <h2>About Marcus Marrio</h2>
            <p>Dummy text is text that is used in the publishing industry or by web designers to occupy the space which will later be filled with &#39;real&#39; content. This is required when, for example, the final text is not yet available. Dummy text is also known as &#39;fill text&#39;. It is said that song composers of the past used dummy texts as lyrics when writing melodies in order to have a &#39;ready-made&#39; text to sing with the melody. Dummy texts have been in use by typesetters since the 16th century.</p>
            <p>Dummy text is text that is used in the publishing industry or by web designers to occupy the space which will later be filled with &#39;real&#39; content. This is required when, for example, the final text is not yet available. Dummy text is also known as &#39;fill text&#39;. It is said that song composers of the past used dummy texts as lyrics when writing melodies in order to have a &#39;ready-made&#39; text to sing with the melody. Dummy texts have been in use by typesetters since the 16th century.</p>
         </div>
      </div>
      <!-- {!! $page->post_content !!}  -->        
   </div>
</div>
@endsection
@section('page-js')
<script>
   @if(session('success'))
       toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', toastr_options);
   @endif
</script>
@endsection