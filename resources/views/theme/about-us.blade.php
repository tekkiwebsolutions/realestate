@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<div class="row mlr0">
   <div class="page_wrapper page-{{ $page->id }}">
      <div class="modern-top-intoduce-section banner">
         <div class="container">
            <div class="row">
               <div class="col-sm-12 col-lg-12">
                  <h2>About Us</h2>
               </div>
            </div>
         </div>
      </div>
      <div class="container">
         <div class="about_us clearfix">
            <div class="col-sm-6">
               <h6>Introduction</h6>
               <h2>Get Started with Property Lookout</h2>
               <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s.</p>
               <p>People are beginning to understand that. However, back over in reality some project schedules and budgets don&rsquo;t allow.</p>
               <div class="learn_more about_more"><a class="btn btn-primary" href="#">Read More</a></div>
            </div>
            <div class="col-sm-6"><img class="img-responsive" src="http://localhost/realestate/assets/img/about_us.jpg" /></div>
         </div>
      </div>
      <div class="company_work">
         <div class="container">
            <div class="row">
               <div class="col-md-3 col-xs-6">
                  <i class="fa fa-home" aria-hidden="true"></i>
                  <div class="col-text">
                     <h4>999</h4>
                     <h5>Complete Project</h5>
                  </div>
               </div>
               <div class="col-md-3 col-xs-6">
                  <i class="fa fa-key" aria-hidden="true"></i>
                  <div class="col-text">
                     <h4>720</h4>
                     <h5>Property Sold</h5>
                  </div>
               </div>
               <div class="col-md-3 col-xs-6">
                  <i class="fa fa-smile-o" aria-hidden="true"></i>
                  <div class="col-text">
                     <h4>450</h4>
                     <h5>Happy Clients</h5>
                  </div>
               </div>
               <div class="col-md-3 col-xs-6">
                  <i class="fa fa-trophy" aria-hidden="true"></i>
                  <div class="col-text">
                     <h4>120</h4>
                     <h5>Awards Win</h5>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="meet_agent">
         <div class="container">
            <h2>MEET OUR AGENT</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta est,<br />
               itaque inventore, eum, hic ullam quos sint dolore doloremque rerum enim?
            </p>
            <div class="row">
               <div class="col-md-3 col-sm-6">
                  <div class="agent-detail">
                     <a href="#"><img src="http://localhost/realestate/assets/img/agent-1.png" /></a>
                     <div class="detail">
                        <h5>Marcus</h5>
                        <span>Company Agent at Property Lookout</span>
                     </div>
                     <ul>
                        <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                     </ul>
                  </div>
               </div>
               <div class="col-md-3 col-sm-6">
                  <div class="agent-detail">
                     <a href="#"><img src="http://localhost/realestate/assets/img/agent-2.png" /></a>
                     <div class="detail">
                        <h5>Marcus</h5>
                        <span>Company Agent at Property Lookout</span>
                     </div>
                     <ul>
                        <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                     </ul>
                  </div>
               </div>
               <div class="col-md-3 col-sm-6">
                  <div class="agent-detail">
                     <a href="#"><img src="http://localhost/realestate/assets/img/agent-3.png" /></a>
                     <div class="detail">
                        <h5>Marcus</h5>
                        <span>Company Agent at Property Lookout</span>
                     </div>
                     <ul>
                        <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                     </ul>
                  </div>
               </div>
               <div class="col-md-3 col-sm-6">
                  <div class="agent-detail">
                     <a href="#"><img src="http://localhost/realestate/assets/img/agent-2.png" /></a>
                     <div class="detail">
                        <h5>Marcus</h5>
                        <span>Company Agent at Property Lookout</span>
                     </div>
                     <ul>
                        <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- {!! $page->post_content !!}  -->        
   </div>
</div>
@endsection
@section('page-js')
<script>
   @if(session('success'))
       toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', toastr_options);
   @endif
</script>
@endsection