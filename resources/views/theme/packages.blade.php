@extends('layout.main')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
@section('main')
<div class="row mlr0">
   <div class="page_wrapper page-{{ $page->id }}">
      <div class="modern-top-intoduce-section banner package_banner">
         <div class="container">
            <div class="row">
               <div class="col-sm-12 col-lg-12">
                  <h2>Choose Packages</h2>
               </div>
            </div>
         </div>
      </div>
      <div class="container">
         <div class="input-group package_email"><span class="input-group-addon"><i class="fa fa-user"></i></span><input class="form-control" name="email" placeholder="Email Address" type="email" /></div>
      </div>
      <div class="pricing_plan">
         <h2><b>Pricing Plan</b></h2>
         <p>Writers and stars of Veep have responded incredulously to the news an Australian politician<br />
            required preinstalled stitches way email client, calendar, mapping program.
         </p>
         <div class="plan_detail">
            <div class="container">
               <div class="row">
                  <div class="col-sm-4">
                     <div class="plans">
                        <h5>Basic</h5>
                        <h4>$199</h4>
                        <h6>Limited Access</h6>
                        <ul>
                           <li>5 apartments</li>
                           <li>1 Blog</li>
                           <li>50 properties</li>
                           <li>Video Limit 3 Minutes</li>
                           <li>Live Notifications</li>
                           <li>Level 1 publicity boost</li>
                        </ul>
                        <div class="select-btn"><a class="btn" href="#">select</a></div>
                     </div>
                  </div>
                  <div class="col-sm-4">
                     <div class="plans">
                        <h5>Advanced</h5>
                        <h4>$299</h4>
                        <h6>Limited Access</h6>
                        <ul>
                           <li>15 apartments</li>
                           <li>3 Blog</li>
                           <li>150 properties</li>
                           <li>Video Limit 5 Minutes</li>
                           <li>Live Notifications</li>
                           <li>Level 1 publicity boost</li>
                        </ul>
                        <div class="select-btn"><a class="btn advance_select" href="#">select</a></div>
                     </div>
                  </div>
                  <div class="col-sm-4">
                     <div class="plans">
                        <h5>Premium</h5>
                        <h4>$249</h4>
                        <h6>Limited Access</h6>
                        <ul>
                           <li>5 Blog</li>
                           <li>Unlimited apartments</li>
                           <li>Unlimited properties</li>
                           <li>Video Limit 7 Minutes</li>
                           <li>Live Noticications</li>
                           <li>Level 3 publicity boost</li>
                        </ul>
                        <div class="select-btn"><a class="btn premium_select" href="#">select</a></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- {!! $page->post_content !!}  -->
   </div>
</div>
@endsection
@section('page-js')
<script>
   @if(session('success'))
       toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', toastr_options);
   @endif
</script>
@endsection